package artemis;

/**
 * Created by brendan on 4/30/15.
 */

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;

import java.text.MessageFormat;
import java.util.Map;

public class MyColourMacro extends BaseMacro implements Macro
{
    public static final String COLOUR_PARAM = "colour";

    @Override
    public boolean hasBody()
    {
        return true;
    }

    @Override
    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        if (StringUtils.isBlank(body))
        {
            return "";
        }

        String[] bodyItems = StringUtils.split(body, ":", 2);
        if (bodyItems.length != 2)
        {
            return body;
        }

        return formatString(bodyItems[0], bodyItems[1]);
    }

    public String formatString(String colour, String body)
    {
        return MessageFormat.format("<span style=\"color: {0};\">{1}</span>", colour, body);
    }

    @Override
    public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        try
        {
            return execute(params, body, (RenderContext) null);
        }
        catch (MacroException e)
        {
            throw new MacroExecutionException(e);
        }
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}